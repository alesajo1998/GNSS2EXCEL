# -*- coding: utf-8 -*-
'''
             Script para convertir GNSS a EXCEL en una sola carpeta. 

                           Servicio Geológico Colombiano 
                                      OVSM 
                               Área de deformación
          Este programa es software libre, para redistribuir y modificar.
   
                                 Requerimientos
 Verificar que se tenga instaladas las librerías: PyPDF2 y xlwt. En caso de no 
     tenerse intaladas se pueden instalar ejecutando los siguientes comandos: 
        
                               pip install PyPDF2
                                pip install xlwt
                              
                             
       Verificar que el script GNSS2EXCEL.py y Gnss_carpeta_correspondiente.py 
                   se encuentre en la misma carpeta que este script.
                                
                Para más información consultar el manual de usuario.
                
                        Autor: Alejandro Salazar González.
                        Autor código original: Joseph B.
      
'''
#Librerías. 

import os.path
import PyPDF2
import xlwt
import os
from datetime import datetime
from time import time
from time import sleep 
#Aquí se encuentra la lista de estaciones que se encuentran disponibles. 
#En caso de tener otra estación disponible simplemente se agrega siguiendo la misma estructura.
lista_estaciones=('afri','agui','bis0','bllr','esme','gual','inde','lagu','lver','mral',
                  'mzal','nere','olle','otun','parm','pira','quim','recm','rode','rubi',
                  'sana','secr','sinn')

#Rutas. 
f = open ('Gnss_config.txt','r')
rut = f.readlines()
f.close()

rp =rut[8] #Leer renglón 8
ruta_pdf=rp.rstrip() #Elimina el salto de línea del renglón 8 y queda la ruta rinex.
re = rut[12]#Leer renglón 12 
ruta_excel=re.rstrip()#Elimina el salto de lína del renglón 12 y queda la ruta final. 
rl=rut[16]#Leer renglón 16
ruta_log=rl.rstrip()#Elimina el salto de lína del renglón 16 y queda la ruta del log.

a=rut[46] #Leer renglón 33
year= a.rstrip('\n') #Elimina el salto de línea y queda el año.
year_end=year[-2:]   #Ultimos dos números del año

sd=rut[37]
Separador_decimales=sd.rstrip()
sm=rut[40]
Separador_miles=sm.rstrip()

numbers=[]  #Vector que contiene los números de cada estación
checked_numbers=[] #Vector que contiene los números rectificados
estaciones=[] #Vector que contiene las estaciones seleccionadas.

lenght=len(lista_estaciones) #Total de las estaciones disponibles
print("    ")
print("                              Conversor de GNSS a EXCEL para carpeta única")
print("    ")
print("                 A continuación podrá ver las estaciones disponibles para el procesamiento")

sleep(1)
print("       ")
print('Afri ----> 0')
print('Agui ----> 1')
print('Bis0 ----> 2')
print('Bllr ----> 3')
print('Esme ----> 4')
print('Gual ----> 5')
print('Inde ----> 6')
print('Lagu ----> 7')
print('Lver ----> 8')
print('Mral ----> 9')
print('Mzal ---> 10')
print('Nere ---> 11')
print('Olle ---> 12')
print('Otun ---> 13')
print('Parm ---> 14')
print('Pira ---> 15')
print('Quim ---> 16')
print('Recm ---> 17')
print('Rode ---> 18')
print('Rubi ---> 19')
print('Sana ---> 20')
print('Secr ---> 21')
print('Sinn ---> 22')
print("       ")
print("Para todas las estaciones ---> t")
print("       ")
print("Digite el número de la estación o de las estaciones que desee procesar separados por una coma.(Por ejemplo 1,2,3) ")
i=input()
longitud=len(i) #Longitud de la lista ingresada por el usuario.

e=0 #Contador
while e < longitud:  
    if i[e] != "t":          #Ciclo que empieza desde 0 hasta la logitud total de la lista.
         if i[e] != ",":               #Si el caracter es diferente a una coma 
             x=e+1                     #Analiza el caracter siguiente
             if x<longitud:            #Si el caracter siguiente existe
                 if i[x] != ",":       #Si el caracter siguiente es diferente a una coma significa que hay un número
                     n=i[e]+i[x]       #Si hay otro número significa que contiene un número de 2 cifras
                     n=int(n)          #Une los dos números y los convierte a enteros.
                     numbers.append(n) #Los agrega al vector de los números
                     e=e+2             #Y suma dos al contador
                 else:
                     n=i[e]            #Sino significa que solo hay un número
                     n=int(n)          #Lo convierte a entero y lo agrega al vector de los números
                     numbers.append(n)
                     e=e+1             #Suma 1 al contador
             else:
                 n=i[e]                #Sino significa que es el último número
                 n=int(n)              #Convierte el valor a entero 
                 numbers.append(n)     #Lo agrega al vector de los números
                 e=e+1                 #Suma 1 al contador
         else:  
             e=e+1 
    else:
        numbers.extend(range(0,lenght)) 
        e=longitud
                 
for b in numbers:
    if b not in checked_numbers and b<lenght: #Pasamos los archivos del vector sin rectificar al rectificado.
        checked_numbers.append(b)             #Esto es por si hay un número repetido
    
for p in checked_numbers:                     #Pasamos todos los números a un nuevo vector con las estaciones correspondientes
    estaciones.append(lista_estaciones[p])

estaciones_print=str(estaciones)
fecha= datetime.now() #Fecha y hora tomada al momento de iniciar el código para el log.
fecha=str(fecha)
TI=time() #Cronómetro para saber la duración del programa.

global resumen
resumen='' #Resumen de los errores que presenta el programa. 

log = open(ruta_log, "a")  #Abre el log.
log.write("Log " + fecha + "\n") #Copia la fecha y la hora en el log
log.close() #Cierra el log

print ("Digite la fecha inicial (Calendario Juliano) ")
dia_inicio = int(input())

print ("Digite la fecha final (Calendario Juliano) ")
dia_fin = int(input())

os.system("cls") #Borra ventana de comandos.

#Copia en el log los días que serán procesados.
log = open(ruta_log, "a") 
log.write("Convirtiendo de GNSS a EXCEL desde el día "+ str(dia_inicio) + " hasta el día " + str(dia_fin) + " "+ "de "+ estaciones_print + "\n")
log.close()

print("Convirtiendo de GNSS a EXCEL desde el día "+ str(dia_inicio) + " hasta el día " + str(dia_fin) + " "+ " de "+ estaciones_print  +"\n")


for estacion in estaciones: 
    cont = 1
    e=estacion.upper()
    Titulo = "DATOS DE LA ESTACIÓN ", e
    wb = xlwt.Workbook()
    ws = wb.add_sheet('Datos de la estación', cell_overwrite_ok=True)
    fmt = xlwt.easyxf
    centrado = fmt('alignment: horiz centre')
    
    ws.write(0, 4, Titulo, centrado)
    ws.write(1, 0, 'FECHA', centrado)
    ws.write(1, 1, 'ESTE', centrado)
    ws.write(1, 2, 'NORTE', centrado)
    ws.write(1, 3, 'HEIGHT', centrado)
    ws.write(1, 4, 'ERR ESTE', centrado)
    ws.write(1, 5, 'ERR NORTE', centrado)
    ws.write(1, 6, 'ERR HEIGHT', centrado)
    ws.write(1, 7, 'DIA', centrado)
    ws.write(1, 8, 'ORBITA', centrado)

    c1=ws.col(0);c1.width=3500 
    c2=ws.col(1);c2.width=3500 
    c3=ws.col(2);c3.width=3500 
    c4=ws.col(3);c4.width=3000 
    c5=ws.col(4);c5.width=3000  
    c6=ws.col(5);c6.width=3500 
    c7=ws.col(6);c7.width=3500 
    c8=ws.col(7);c8.width=2500 
    c9=ws.col(8);c9.width=3000
    for x in range(dia_inicio,dia_fin+1):
        pdf2 = ""
        cont = cont + 1
        num = str(x)
        if x < 10:
            a = '00'
        if (x < 100) & (x >= 10):
            a = '0'
        if x >= 100:
            a = ''
        num = a + num;

        pdf2 = ruta_pdf + estacion + num + '0' + '.pdf'

        if os.path.exists(pdf2):
            doc = ""
            doc = PyPDF2.PdfFileReader(pdf2, 'rb')
            print(pdf2)
            page = doc.getPage(0)
            page1 = page.extractText()
            pos_la = page1.find('Sigmas(')
            pos_dat = page1.find('UTM (North)')
            pos_orbita = page1.find('NRCan Rapid')

            if pos_orbita == -1:
                orbita = "Final"
            else:
                orbita = "Rápida"
            pos_orbita = page1.find('NRCan Ultra-rapid')
            if pos_orbita != -1:
                orbita = "Rápida"

            pag = page1[226:254]
            fecha = page1[94:104]
            dato_este = page1[pos_dat+37:pos_dat+47]
            dato_norte = page1[pos_dat+20:pos_dat+30]
            dato_height = page1[pos_la-11:pos_la-3]
            dato_ernorte = page1[pos_la+12:pos_la+17]
            dato_ereste = page1[pos_la+20:pos_la+25]
            dato_erheight = page1[pos_la+28:pos_la+33]
        

        
            if Separador_miles in dato_este:
                dato_este=dato_este.replace(Separador_miles,Separador_decimales)
            
            if Separador_miles in dato_norte:
                dato_norte=dato_norte.replace(Separador_miles,Separador_decimales) 
            
            if Separador_miles in dato_height:
                dato_height=dato_height.replace(Separador_miles,Separador_decimales)  
            
            if Separador_miles in dato_ereste:
                dato_ereste=dato_ereste.replace(Separador_miles,Separador_decimales) 
           
            if Separador_miles in dato_ernorte:
                dato_ernorte=dato_ernorte.replace(Separador_miles,Separador_decimales)
            
            if Separador_miles in dato_erheight:
                dato_erheight=dato_erheight.replace(Separador_miles,Separador_decimales)  
                                               
            print("Fecha: ",fecha)
            print("Dato norte: ",dato_norte)
            print("Dato este: ", dato_este)
            print("Dato height: ",dato_height)
            print("Error norte: ", dato_ernorte)
            print("Error este: ",dato_ereste)
            print("Error height: ",dato_erheight)
            print("-------------------------------------------------------------")

            ws.write(cont, 0, (fecha), centrado)
            ws.write(cont, 1, (dato_este), centrado)
            ws.write(cont, 2, (dato_norte), centrado)
            ws.write(cont, 3, (dato_height), centrado)
            ws.write(cont, 4, (dato_ereste), centrado)
            ws.write(cont, 5, (dato_ernorte), centrado)
            ws.write(cont, 6, (dato_erheight), centrado)
            ws.write(cont, 8, (orbita), centrado)
            ws.write(cont, 7, str(x), centrado)
            ruta_final= ruta_excel+ estacion + '.xls'
            wb.save(ruta_final)
            if os.path.isfile(ruta_final):
                log = open(ruta_log, "a")  #Copia el registro en el log.
                log.write('Conversión exitosa de: ' + estacion + num + '0' + '.pdf' + "\n")
                log.close()  
            else: 
                log = open(ruta_log, "a")  #Copia el registro en el log.
                log.write('Error en : ' + estacion + num + '0' + '.pdf' + "\n")
                log.close()   
                resumen = resumen + estacion + num + '0' + '.pdf '

        else:
            log = open(ruta_log, "a")  #Copia el registro en el log.
            log.write('Error en : ' + estacion + num + '0' + '.pdf' + "\n")
            log.close()  
            resumen = resumen + estacion + num + '0' + '.pdf '
            print("No existe" + " : " + pdf2)
            cont=cont-1

if resumen != '':   
    log = open(ruta_log, "a")  #Copia el registro en el log.
    log.write('En resumen se encontraron problemas en: ' + resumen + "\n" +  "\n")
    print('En resumen se encontraron problemas en: ' + resumen) 
    log.close() 
else:
    log = open(ruta_log, "a")  #Copia el registro en el log.
    log.write('En resumen no se encontraron problemas.' + "\n" +  "\n")
    print('En resumen no se encontraron problemas.') 
    log.close()            