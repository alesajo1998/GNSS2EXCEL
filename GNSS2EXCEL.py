# -*- coding: utf-8 -*-

'''
Script que se encarga de unificar las rutinas conversión de archivos GNSS a EXCEL. 


                           Servicio Geológico Colombiano 
                                      OVSM 
                               Área de deformación
          Este programa es software libre, para redistribuir y modificar.
   
                                 Requerimientos
 Verificar que se tenga instaladas las librerías: PyPDF2 y xlwt. En caso de no 
     tenerse intaladas se pueden instalar ejecutando los siguientes comandos: 
        
                               pip install PyPDF2
                                pip install xlwt
                              
                             
    Verificar que el script Gnss_carpeta_unica.py y Gnss_carpeta_correspondiente.py 
           y Gnss_config.txt se encuentre en la misma carpeta que este script.
                                
                Para más información consultar el manual de usuario.
                
                        Autor: Alejandro Salazar González.
                        Autor código original: Joseph B.
      
'''
import os
from time import sleep 

os.system ("cls") 

print("    ")
print("                                       Servicio Geológico Colombiano")
print("                                                   OVSM")
print("    ")
print("                                     Conversor de archivos GNSS a EXCEL")
print("    ")
print("                    A continuación podrá ver las opciones disponibles para el procesamiento")
sleep(2)


print("       ")
print('Para convertir los archivos que se encuentran en una sola carpeta -----------------------------> 0')
print('Para convertir los archivos que se encuentran distribuidos en sus carpetas correspondientes ---> 1')

print("       ")

print("Digite su respuesta ")
i=input()  #Valor digitado por el usuario.
i=int(i)

if i==0:
    os.system("python Gnss_carpeta_unica.py") #Si la respuesta es 0 ejecuta el código de carpeta única
elif i==1:
    os.system("python Gnss_carpeta_correspondiente.py") #Si la respuesta es 1 ejecuta el código de carpeta correspondiente
else: 
    print("No se reconoció la respuesta, por favor intente nuevamente.")   
    exit()